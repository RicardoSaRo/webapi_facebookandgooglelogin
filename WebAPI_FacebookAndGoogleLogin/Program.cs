using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Microsoft.Extensions.Configuration.UserSecrets;
using WebAPI_FacebookAndGoogleLogin.Data;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services; //--> Needed
var configuration = builder.Configuration; //--> Needed

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddControllersWithViews();

//--> SERVICE FOR FACEBOOK AUTHENTICATION (Needs NuGet installation of "Core.Authentication.Faceebook"
//--> IMPORTANT: Introduce the following commands in terminal to keep hidden the sensitive info in the "user-secrets"
//--> dotnet user-secrets set "Authentication:Facebook:AppId" "(app id string)" --project (project name here)
//--> dotnet user-secrets set "Authentication:Facebook:AppSecret" "(app secret string)" --project (project name here)
services.AddAuthentication()
    .AddFacebook(facebookOptions =>
{
    facebookOptions.AppId = configuration["Authentication:Facebook:AppId"];
    facebookOptions.AppSecret = configuration["Authentication:Facebook:AppSecret"];

//--> SERVICE FOR GOOGLE AUTHENTICATION (Needs NuGet installation of "Core.Authentication.Google"
//--> IMPORTANT: Introduce the following commands in terminal to keep hidden the sensitive info in the "user-secrets"
//--> dotnet user-secrets set "Authentication:Google:ClientId" "(client id string)" --project (project name here)
//--> dotnet user-secrets set "Authentication:Google:ClientSecret" "(client secret string)" --project (project name here)
})  .AddGoogle(googleOptions =>
{
    googleOptions.ClientId = configuration["Authentication:Google:ClientId"];
    googleOptions.ClientSecret = configuration["Authentication:Google:ClientSecret"];
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
